import discord
from discord.ext import commands


class ModCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # Starting event

    @commands.Cog.listener()
    async def on_ready(self):
        print('Mod Commands Cog successfully loaded.')

    # --- COMMANDS ---

    @commands.command()
    async def purgeAll(self, ctx, arg1, arg2):
        role_names = [role.name for role in ctx.author.roles]
        if 'Właściciel' in role_names or 'Admin' in role_names:
            count = 0
            for channel in ctx.guild.text_channels:
                try:
                    mgs = []
                    async for x in channel.history(limit=int(arg1)):
                        obj_role_names = [role.name for role in ctx.guild.get_member(
                            int(arg2[3:len(arg2)-1])).roles]
                        if str(x.author.id) == arg2[3:len(arg2)-1] and not('Szefowa Szefa Właściciela' in obj_role_names or 'Szef Właściciela' in obj_role_names or 'Administrator' in obj_role_names or 'Moderator' in obj_role_names or 'Wlasciciel' in obj_role_names or 'Bot' in obj_role_names):
                            mgs.append(x)
                            count += 1
                    await channel.delete_messages(mgs)
                except:
                    print(f'{channel} Przecież tu nic nie ma!')
            await ctx.send(f'Usunięto {count} wiadomości')
        else:
            await ctx.send('Nie masz uprawnień do używania tej komendy!')


def setup(bot):
    bot.add_cog(ModCommands(bot))
