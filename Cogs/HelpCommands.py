import json
import discord
from discord.ext import commands


class HelpCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # Starting event
    @commands.Cog.listener()
    async def on_ready(self):
        print('Help Commands Cog successfully loaded.')

    # Opening Data
    @staticmethod
    def get_data():
        with open('JsonData/data1.json')as jfile:
            data = json.load(jfile)
            temp = data
            jfile.close()

    # saving Data
    @staticmethod
    def write_json(data, filename='JsonData/data1.json'):
        with open(filename, 'w') as jfile:
            json.dump(data, jfile, indent=4)
            jfile.close()

    # checking if user exists in data
    @staticmethod
    def json_check(authorID):
        with open('JsonData/data1.json')as jfile:
            data = json.load(jfile)
            for dataID in data:
                if(dataID['userID'] == authorID):
                    return True
            jfile.close()
            return False

    # --- COMMANDS ---

    @commands.command()
    async def zamknij(self, ctx):
        with open('JsonData/data1.json')as jfile:
            data = json.load(jfile)
            new_data = []
            guild = ctx.guild
            for elem in data:
                if elem['channelID'] == ctx.message.channel.id:
                    role_names = [role.name for role in ctx.author.roles]
                    if 'Właściciel' in role_names or 'Admin' in role_names or '👥￤Ekipa' in role_names or '👶￤Młoda Ekipa' in role_names:
                        channel = discord.utils.get(
                            guild.channels, id=elem['channelID'])
                        await channel.delete()
                    else:
                        new_data.append(elem)
                        channel = ctx.message.channel
                        await channel.send('Nie masz uprawnień! ' + ctx.author.mention)
                else:
                    new_data.append(elem)

            self.write_json(new_data)
            jfile.close()

    @commands.command()
    async def pomoc(self, ctx):
        with open('JsonData/data1.json')as jfile:
            data = json.load(jfile)
            temp = data
            if ctx.channel.id == 938896718630641754:
                if self.json_check(ctx.message.author.id) == False:
                    guild = ctx.guild
                    user = ctx.message.author
                    adm = discord.utils.get(
                        ctx.guild.roles, name='Admin')
                    ekipa = discord.utils.get(ctx.guild.roles, name='👥￤Ekipa')
                    mEkipa = discord.utils.get(
                        ctx.guild.roles, name='👶￤Młoda Ekipa')
                    await ctx.send('Twój prywatny kanał pomocy został stworzony!')
                    overwrites = {
                        guild.default_role: discord.PermissionOverwrite(read_messages=False),
                        guild.me: discord.PermissionOverwrite(read_messages=True),
                        user: discord.PermissionOverwrite(read_messages=True),
                        adm: discord.PermissionOverwrite(read_messages=True),
                        ekipa: discord.PermissionOverwrite(read_messages=True),
                        mEkipa: discord.PermissionOverwrite(read_messages=True)
                    }
                    channel = await guild.create_text_channel(name=f'🚑Pomoc {ctx.message.author.name}', category=guild.get_channel(938895625334980720), overwrites=overwrites)
                    y = {"userID": ctx.message.author.id,
                         "channelID": channel.id}
                    temp.append(y)
                    self.write_json(temp)
                    await channel.send(ctx.message.author.mention+' opisz swoją sprawę, a wkrótce ktoś się nią zajmie 🧐')
                    await channel.send(' 1. Nie pisz "mam pytanie" - zadaj je od razu.\n 2. Podaj swój nick w MC \n 3. Opisz od razu dokładnie sprawę w jakiej piszesz\n 4. Jeśli masz jakieś dowody w sprawie której piszesz - wyślij je od razu\n 5. Nie oznaczaj całej administracji - ktoś odpowie Ci najszybciej jak to możliwe')
                else:
                    await ctx.send("Masz już kanał pomocy!")
            else:
                await ctx.send("Użyj tej komendy na kanale #⏳stworz-ticketa !")

            jfile.close()


def setup(bot):
    bot.add_cog(HelpCommands(bot))
