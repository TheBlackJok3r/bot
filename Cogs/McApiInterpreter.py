import json
from aiohttp import request
import discord
import requests
from discord.ext import commands


class McApiInterpreter(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print('McApiInterpreter Cog successfully loaded.')

    # Opening Data
    @staticmethod
    def get_data(filename='JsonData/data1.json'):
        with open(filename)as jfile:
            data = json.load(jfile)
            temp = data
            jfile.close()

    # saving Data
    @staticmethod
    def write_json(data, filename='JsonData/data1.json'):
        with open(filename, 'w') as jfile:
            json.dump(data, jfile, indent=4)
            jfile.close()

    # checking if user exists in data
    @staticmethod
    def json_check(arg, param='userID', filename='JsonData/data1.json'):
        with open(filename)as jfile:
            data = json.load(jfile)
            for d in data:
                if(d[param] == arg):
                    return True
            jfile.close()
            return False

    # --- COMMANDS ---

    @commands.command()
    async def dodaj(self, ctx, nickMc):
        print('Forging add request...')
        with open('JsonData/McApi.json') as jfile:
            data = json.load(jfile)
            temp = data
            try:
                if ctx.channel.id == 938896122846539817:
                    if self.json_check(ctx.message.author.id, 'userID', 'JsonData/McApi.json') == False:
                        if self.json_check(nickMc, 'nickMc', 'JsonData/McApi.json') == False:
                            r = requests.post(
                                f'link')
                            print(r)
                            y = {"userID": ctx.message.author.id,
                                 "nickMc": nickMc}
                            await ctx.channel.send(ctx.message.author.mention + 'Zrobione byku')
                            temp.append(y)
                            self.write_json(temp, 'JsonData/McApi.json')
                            print('Forged')
                        else:
                            await ctx.channel.send(ctx.message.author.mention + 'Takie konto jest już wpisane')
                            print('Stopped')
                    else:
                        await ctx.channel.send(ctx.message.author.mention + 'Już wpisałeś swoje konto')
                        print('Stopped')
                else:
                    await ctx.channel.send(ctx.message.author.mention + 'Nie ten kanał')
                    print('Stopped')
            except:
                print('Failed')
                await ctx.channel.send('Serwer się zepsuł sadeg')
            jfile.close()

    # FOR MODS ONLY
    @commands.command()
    async def usun(self, ctx, nickMc):
        print('Forging delete request...')
        with open('JsonData/McApi.json') as jfile:
            data = json.load(jfile)
            temp = data
            try:
                if ctx.channel.id == 938896122846539817:
                    for d in data:
                        y = {"userID": ctx.message.author.id,
                             "nickMc": nickMc}
                        if d == y:
                            r = requests.post(
                                f'link')
                            print(r)
                            temp.remove(y)
                            self.write_json(temp, 'JsonData/McApi.json')
                            print('Forged')
                            await ctx.channel.send(ctx.message.author.mention+'Ten nick został usunięty z whitelisty')
                            break
                else:
                    await ctx.channel.send(ctx.message.author.mention+'Nie możesz usunąć tego nicku, lub nie jesteś jeszcze wpisany na whiteliste')
                    print('Stopped')
            except:
                print('Failed')
                await ctx.channel.send('Serwer się zepsuł sadeg')
            jfile.close()


def setup(bot):
    bot.add_cog(McApiInterpreter(bot))
