import json
from operator import contains
from attr import has
import discord
import os
import asyncio
import datetime
import pytz
import requests
from discord.ext import commands
from discord.utils import get
from googleapiclient.discovery import build
from discord.ext import commands, tasks


intents = discord.Intents.all()
bot = commands.Bot(command_prefix=".", intents=intents)
bot.remove_command('help')

# STARTING EVENT


@bot.event
async def on_ready():
    await bot.change_presence(status=discord.Status.online, activity=discord.Game('subowice.pl'))
    print('Bot is ready')

# Opening Data


def get_data(filename='JsonData/data1.json'):
    with open(filename)as jfile:
        data = json.load(jfile)
        temp = data
        jfile.close()

# saving Data


def write_json(data, filename='JsonData/data1.json'):
    with open(filename, 'w') as jfile:
        json.dump(data, jfile, indent=4)
        jfile.close()

# checking if user exists in data


def json_check(arg, param='userID', filename='JsonData/data1.json'):
    with open(filename)as jfile:
        data = json.load(jfile)
        for d in data:
            if(d[param] == arg):
                return True
        jfile.close()
        return False

# --- TASKS ---


@tasks.loop(hours=24)
async def checkSub():
    with open('JsonData/McApi.json')as jfile:
        print('Forging CheckSub request...')
        data = json.load(jfile)
        temp = data
        guild = bot.get_guild(884551452847136818)
        for elem in data:
            player = elem['nickMc']
            try:
                # Check if player has sub
                member_id = guild.get_member(elem['userID'])
                roles = [role.name for role in member_id.roles]
                if not '💜￤Twitch Sub' in roles:
                    r = requests.post(
                        f'link')
                    print(r)
                    temp.remove(elem)
                    continue
                # Check player ranks
                if not'💪😎🤙￤OmegaGigaChad' in roles:
                    rTier3 = requests.post(
                        f'link')
                    print(rTier3)
                if not '😎🤙￤GigaChad' in roles:
                    rTier2 = requests.post(
                        f'link')
                    print(rTier2)
            except:
                # Player is not on server, which results in kick from wl
                print('Exception - player not on server')
                r = requests.post(
                    f'link')
                print(r)
                temp.remove(elem)
        write_json(temp, 'JsonData/McApi.json')
        print('Forged')
        jfile.close()


checkSub.start()

# Bot Cog
bot.load_extension('Cogs.ModCommands')
bot.load_extension('Cogs.HelpCommands')
bot.load_extension('Cogs.McApiInterpreter')

bot.run('')
